class TodosController < ApplicationController
	skip_before_action :verify_authenticity_token
	
	def create
		@todo = Todo.new(todo_params)
		@todo.save!
	end
	
	def update
		@todo = Todo.find(params[:id])
		@todo.update(todo_params)
	end
	
	private
	
	def todo_params
		params.permit(:text, :project_id, :isCompleted)
	end
end
