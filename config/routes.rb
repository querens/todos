Rails.application.routes.draw do
# 	get 'projects', to: 'projects#index', as: 'projects'

	root 'projects#index'
	
	resources :projects, only: [:index] do
		resources :todos, only: [:update]
	end
	resources :todos, only: [:create]
end
